# Neon device image pipeline

This repo contains files to create neon-based images for various devices
using debos.

## Preparation

Debos can be used from a docker image directly like this:

```
docker run --rm --interactive --tty --device /dev/kvm --workdir /recipes --mount "type=bind,source=$(pwd),destination=/recipes" --security-opt label=disable godebos/debos
```
(Note that this requires kvm/virtualization to be enabled in BIOS/uefi and your user to be in groups kvm and docker, and don't forget to log out/restart after adding groups)
To make this more convenient you can add a bash alias like this to your `.bashrc`

```
alias debos-docker="docker run --rm --interactive --tty --device /dev/kvm --workdir /recipes --mount \"type=bind,source=\$(pwd),destination=/recipes\" --security-opt label=disable godebos/debos"
```

## Building images

So to build a Plasma Mobile image for the PinePhone you can run

```
debos-docker base.yml -t "device:pinephone" -t "variant:plamo"
```

## Further documentation

https://github.com/go-debos/debos
